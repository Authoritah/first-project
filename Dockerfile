FROM python:alpine
ENV FLASK_APP=main.py
RUN mkdir FlaskApp
WORKDIR FlaskApp
COPY . /FlaskApp
RUN pip install --no-cache-dir -r requirements.txt
CMD ["flask", "run", "--host=0.0.0.0"]

